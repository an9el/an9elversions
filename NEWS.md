# an9elversions 0.3.0

# an9elversions 0.2.1

# an9elversions 0.2.0

# an9elversions 0.1.0

# an9elversions 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
* Added support for HASH, checking if the project file changes.
* Added `check_last_version_online` to centralize checking on versions
