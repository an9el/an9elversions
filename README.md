
---
title: an9elversions package
author: Angel Martinez-Perez
institute: Unit of Genomic of Complex Diseases
date: "Started: 2021-12-01"
description: R package to keep track of the different projects and locations of the data
last update: "2023-11-21"
---

an9elversions <a href='https://gitlab.com/an9el/an9elversions'><img src='inst/figures/logo_an9elversions.png' align='right' height='138' /></a><!-- social: start -->
[![](https://info.orcid.org/wp-content/uploads/2019/11/orcid_24x24.png)](https://orcid.org/0000-0002-5934-1454 'ORCID')[![](https://icons-for-free.com/download-icon-linkedin+network+social+icon-1320086082965629906_24.png)](https://es.linkedin.com/in/angel-martinez-perez-28116a9 'linkedin')[![](https://icons-for-free.com/download-icon-super+tiny+icons+scholar-1324450734638440291_24.png)](http://scholar.google.es/citations?user=IleVNpwAAAAJ 'google scholar')[![](https://icons-for-free.com/download-icon-super+tiny+icons+researchgate-1324450767242972063_24.png)](https://www.researchgate.net/profile/Angel_Martinez-Perez/publications/ 'researchgate')[![](https://www.dovepress.com/assets/img/logo_pubmed.png)](https://www.ncbi.nlm.nih.gov/myncbi/1liNzajtgcWkjb/bibliography/public/ 'pubmed')[![](https://cdn.exclaimer.com/Handbook%20Images/twitter-icon_square_24x24.png)](https://twitter.com/an9el_mp 'twitter')[<img src="https://pbs.twimg.com/profile_images/791689256746029060/GcrkL5iq_400x400.jpg" width="24" height="24"/>](http://www.recercasantpau.cat/es/grupo/genomica-y-bioinformatica-de-enfermedades-de-base-genetica-compleja/ "IIB Sant Pau")[<img src="https://media.glassdoor.com/sql/1556432/clarivate-squarelogo-1595527924052.png" width="24" height="24"/>](https://www.webofscience.com/wos/author/record/2108096 "WoS")
<!-- social: end -->



# Installation



```r
devtools::install_gitlab("an9el/an9elversions")
```


# GOAL

1. This package registry the list of the projects and versions.   
1. This package retrieve the online versions   
1. This package retrieve the list of projects, servers and locations within those servers   
1. This package works in collaboration with the package [an9elproject](https://gitlab.com/an9el/an9elproject)

# Warning

Everytime that a project is updated needs to be commited and push to
`origin/main`. Otherwise the project may not be tagged.

# SESSION INFO


<details closed>
<summary> <span title='Clik to Expand'> Current session info </span> </summary>

```r

─ Session info ─────────────────────────────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.3.2 (2023-10-31)
 os       Debian GNU/Linux 12 (bookworm)
 system   x86_64, linux-gnu
 ui       X11
 language en_US:en
 collate  en_US.UTF-8
 ctype    en_US.UTF-8
 tz       Europe/Madrid
 date     2023-11-21
 pandoc   2.17.1.1 @ /usr/bin/ (via rmarkdown)

─ Packages ─────────────────────────────────────────────────────────────────────────────────────────
 ! package       * version    date (UTC) lib source
   an9elutils    * 0.6.0      2023-09-29 [1] gitlab (an9el/an9elutils@d0e02db)
 P an9elversions * 0.3.0      2023-09-29 [?] gitlab (an9el/an9elversions@8bc3e6b)
   backports       1.4.1      2021-12-13 [1] CRAN (R 4.3.0)
   base64enc       0.1-3      2015-07-28 [1] CRAN (R 4.3.0)
   bibtex          0.5.1      2023-01-26 [1] CRAN (R 4.3.0)
   broom         * 1.0.5      2023-06-09 [1] CRAN (R 4.3.1)
   cachem          1.0.8      2023-05-01 [1] CRAN (R 4.3.0)
   callr           3.7.3      2022-11-02 [1] CRAN (R 4.3.0)
   caret           6.0-94     2023-03-21 [1] CRAN (R 4.3.0)
   checkmate       2.3.0      2023-10-25 [1] CRAN (R 4.3.2)
   class           7.3-22     2023-05-03 [1] CRAN (R 4.3.0)
   cli             3.6.1      2023-03-23 [1] CRAN (R 4.3.0)
   clipr           0.8.0      2022-02-22 [1] CRAN (R 4.3.0)
   codetools       0.2-19     2023-02-01 [1] CRAN (R 4.3.0)
   collapse        2.0.3      2023-10-17 [1] CRAN (R 4.3.2)
   colorspace      2.1-0      2023-01-23 [1] CRAN (R 4.3.0)
   commonmark      1.9.0      2023-03-17 [1] CRAN (R 4.3.0)
   crayon          1.5.2      2022-09-29 [1] CRAN (R 4.3.0)
   data.table    * 1.14.8     2023-02-17 [1] CRAN (R 4.3.0)
   desc            1.4.2      2022-09-08 [1] CRAN (R 4.3.0)
   details       * 0.3.0      2022-03-27 [1] CRAN (R 4.3.0)
   devtools        2.4.5      2022-10-11 [1] CRAN (R 4.3.0)
   digest          0.6.33     2023-07-07 [1] CRAN (R 4.3.1)
   dplyr         * 1.1.3      2023-09-03 [1] CRAN (R 4.3.1)
   DT            * 0.30       2023-10-05 [1] CRAN (R 4.3.2)
   ellipsis        0.3.2      2021-04-29 [1] CRAN (R 4.3.0)
   evaluate        0.23       2023-11-01 [1] CRAN (R 4.3.2)
   fansi           1.0.5      2023-10-08 [1] CRAN (R 4.3.2)
   fastmap         1.1.1      2023-02-24 [1] CRAN (R 4.3.0)
   forcats       * 1.0.0      2023-01-29 [1] CRAN (R 4.3.0)
   foreach         1.5.2      2022-02-02 [1] CRAN (R 4.3.0)
   fs              1.6.3      2023-07-20 [1] CRAN (R 4.3.1)
   future          1.33.0     2023-07-01 [1] CRAN (R 4.3.1)
   future.apply    1.11.0     2023-05-21 [1] CRAN (R 4.3.0)
   generics        0.1.3      2022-07-05 [1] CRAN (R 4.3.0)
   ggplot2       * 3.4.4      2023-10-12 [1] CRAN (R 4.3.2)
   globals         0.16.2     2022-11-21 [1] CRAN (R 4.3.0)
   glue          * 1.6.2      2022-02-24 [1] CRAN (R 4.3.0)
   gower           1.0.1      2022-12-22 [1] CRAN (R 4.3.0)
   gridExtra       2.3        2017-09-09 [1] CRAN (R 4.3.0)
   gtable          0.3.4      2023-08-21 [1] CRAN (R 4.3.1)
   hardhat         1.3.0      2023-03-30 [1] CRAN (R 4.3.0)
   here          * 1.0.1      2020-12-13 [1] CRAN (R 4.3.0)
   hms             1.1.3      2023-03-21 [1] CRAN (R 4.3.0)
   htmltools       0.5.7      2023-11-03 [1] CRAN (R 4.3.2)
   htmlwidgets   * 1.6.2      2023-03-17 [1] CRAN (R 4.3.0)
   httpuv          1.6.12     2023-10-23 [1] CRAN (R 4.3.2)
   httr            1.4.7      2023-08-15 [1] CRAN (R 4.3.1)
   ipred           0.9-14     2023-03-09 [1] CRAN (R 4.3.0)
   iterators       1.0.14     2022-02-05 [1] CRAN (R 4.3.0)
   jsonlite        1.8.7      2023-06-29 [1] CRAN (R 4.3.1)
   kableExtra    * 1.3.4      2021-02-20 [1] CRAN (R 4.3.0)
   knitcitations * 1.0.12     2021-01-10 [1] CRAN (R 4.3.0)
   knitr         * 1.45       2023-10-30 [1] CRAN (R 4.3.2)
   labeling        0.4.3      2023-08-29 [1] CRAN (R 4.3.1)
   later           1.3.1      2023-05-02 [1] CRAN (R 4.3.0)
   lattice         0.22-5     2023-10-24 [1] CRAN (R 4.3.2)
   lava            1.7.2.1    2023-02-27 [1] CRAN (R 4.3.0)
   lifecycle       1.0.3      2022-10-07 [1] CRAN (R 4.3.0)
   listenv         0.9.0      2022-12-16 [1] CRAN (R 4.3.0)
   lubridate     * 1.9.3      2023-09-27 [1] CRAN (R 4.3.1)
   magick          2.8.1      2023-10-22 [1] CRAN (R 4.3.2)
   magrittr      * 2.0.3      2022-03-30 [1] CRAN (R 4.3.0)
   MASS            7.3-60     2023-05-04 [1] CRAN (R 4.3.0)
   Matrix          1.6-1.1    2023-09-18 [1] CRAN (R 4.3.1)
   matrixStats     1.0.0      2023-06-02 [1] CRAN (R 4.3.1)
   memoise         2.0.1      2021-11-26 [1] CRAN (R 4.3.0)
   mime            0.12       2021-09-28 [1] CRAN (R 4.3.0)
   miniUI          0.1.1.1    2018-05-18 [1] CRAN (R 4.3.0)
   ModelMetrics    1.2.2.2    2020-03-17 [1] CRAN (R 4.3.0)
   mtb           * 0.1.8      2022-10-20 [1] CRAN (R 4.3.0)
   munsell         0.5.0      2018-06-12 [1] CRAN (R 4.3.0)
   nlme            3.1-163    2023-08-09 [1] CRAN (R 4.3.1)
   nnet            7.3-19     2023-05-03 [1] CRAN (R 4.3.0)
   pacman        * 0.5.1      2019-03-11 [1] CRAN (R 4.3.0)
   pander          0.6.5      2022-03-18 [1] CRAN (R 4.3.0)
   parallelly      1.36.0     2023-05-26 [1] CRAN (R 4.3.1)
   pillar          1.9.0      2023-03-22 [1] CRAN (R 4.3.0)
   pkgbuild        1.4.2      2023-06-26 [1] CRAN (R 4.3.1)
   pkgconfig       2.0.3      2019-09-22 [1] CRAN (R 4.3.0)
   pkgload         1.3.3      2023-09-22 [1] CRAN (R 4.3.1)
   plyr          * 1.8.9      2023-10-02 [1] CRAN (R 4.3.1)
   png             0.1-8      2022-11-29 [1] CRAN (R 4.3.0)
   prettyunits     1.2.0      2023-09-24 [1] CRAN (R 4.3.1)
   pROC            1.18.5     2023-11-01 [1] CRAN (R 4.3.2)
   processx        3.8.2      2023-06-30 [1] CRAN (R 4.3.1)
   prodlim         2023.08.28 2023-08-28 [1] CRAN (R 4.3.1)
   profvis         0.3.8      2023-05-02 [1] CRAN (R 4.3.0)
   promises        1.2.1      2023-08-10 [1] CRAN (R 4.3.1)
   pryr            0.1.6      2023-01-17 [1] CRAN (R 4.3.0)
   ps              1.7.5      2023-04-18 [1] CRAN (R 4.3.0)
   purrr         * 1.0.2      2023-08-10 [1] CRAN (R 4.3.1)
   R6              2.5.1      2021-08-19 [1] CRAN (R 4.3.0)
   rapportools     1.1        2022-03-22 [1] CRAN (R 4.3.0)
   Rcpp            1.0.11     2023-07-06 [1] CRAN (R 4.3.1)
   readr         * 2.1.4      2023-02-10 [1] CRAN (R 4.3.0)
   recipes         1.0.8      2023-08-25 [1] CRAN (R 4.3.1)
   RefManageR      1.4.0      2022-09-30 [1] CRAN (R 4.3.0)
   remotes         2.4.2.1    2023-07-18 [1] CRAN (R 4.3.1)
   reshape2        1.4.4      2020-04-09 [1] CRAN (R 4.3.0)
   rlang           1.1.2      2023-11-04 [1] CRAN (R 4.3.2)
   rmarkdown     * 2.25       2023-09-18 [1] CRAN (R 4.3.1)
   roxygen2        7.2.3      2022-12-08 [1] CRAN (R 4.3.0)
   rpart           4.1.21     2023-10-09 [1] CRAN (R 4.3.2)
   rprojroot       2.0.3      2022-04-02 [1] CRAN (R 4.3.0)
   rstudioapi      0.15.0     2023-07-07 [1] CRAN (R 4.3.1)
   rvest           1.0.3      2022-08-19 [1] CRAN (R 4.3.0)
   scales          1.2.1      2022-08-20 [1] CRAN (R 4.3.0)
   sessioninfo     1.2.2      2021-12-06 [1] CRAN (R 4.3.0)
   shiny           1.7.5.1    2023-10-14 [1] CRAN (R 4.3.2)
   stringi         1.7.12     2023-01-11 [1] CRAN (R 4.3.0)
   stringr       * 1.5.0      2022-12-02 [1] CRAN (R 4.3.0)
   summarytools  * 1.0.1      2022-05-20 [1] CRAN (R 4.3.0)
   survival        3.5-7      2023-08-14 [1] CRAN (R 4.3.1)
   svglite         2.1.2      2023-10-11 [1] CRAN (R 4.3.2)
   systemfonts     1.0.5      2023-10-09 [1] CRAN (R 4.3.2)
   tibble        * 3.2.1      2023-03-20 [1] CRAN (R 4.3.0)
   tidyr         * 1.3.0      2023-01-24 [1] CRAN (R 4.3.0)
   tidyselect      1.2.0      2022-10-10 [1] CRAN (R 4.3.0)
   tidyverse     * 2.0.0      2023-02-22 [1] CRAN (R 4.3.0)
   timechange      0.2.0      2023-01-11 [1] CRAN (R 4.3.0)
   timeDate        4022.108   2023-01-07 [1] CRAN (R 4.3.0)
   tzdb            0.4.0      2023-05-12 [1] CRAN (R 4.3.0)
   urlchecker      1.0.1      2021-11-30 [1] CRAN (R 4.3.0)
   usethis         2.2.2      2023-07-06 [1] CRAN (R 4.3.1)
   utf8            1.2.4      2023-10-22 [1] CRAN (R 4.3.2)
   vctrs           0.6.4      2023-10-12 [1] CRAN (R 4.3.2)
   viridis         0.6.4      2023-07-22 [1] CRAN (R 4.3.1)
   viridisLite     0.4.2      2023-05-02 [1] CRAN (R 4.3.0)
   visNetwork      2.1.2      2022-09-29 [1] CRAN (R 4.3.0)
   webshot       * 0.5.5      2023-06-26 [1] CRAN (R 4.3.1)
   withr           2.5.2      2023-10-30 [1] CRAN (R 4.3.2)
   xfun            0.41       2023-11-01 [1] CRAN (R 4.3.2)
   xml2            1.3.5      2023-07-06 [1] CRAN (R 4.3.1)
   xtable          1.8-4      2019-04-21 [1] CRAN (R 4.3.0)
   yaml            2.3.7      2023-01-23 [1] CRAN (R 4.3.0)

 [1] /usr/local/lib/R/site-library
 [2] /usr/lib/R/site-library
 [3] /usr/lib/R/library

 P ── Loaded and on-disk path mismatch.

────────────────────────────────────────────────────────────────────────────────────────────────────

```

</details>
<br>

