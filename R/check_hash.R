
##' check if the project/version matches the hash or create hash if new
##'
##' check if the project/version matches the hash or create hash if new
##' @title check if the project/version matches the hash or create hash if new
##' @param project \code{an9elproject} object to check the hash
##' @param verbose show information
##' @return TRUE if the project exist and has the same HASH.FALSE if the project
##' previously exists and the HASH do not match. Or return a new hash if the project
##' doesn't exists.
##' @examples check_hash('Naive')
##' @author person("Angel", "Martinez-Perez",
##' email = \email{angelmartinez@pm.me},
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' 
##' @export
check_hash <- function(project, verbose = FALSE) {
    checkmate::assertFlag(verbose)
    checkmate::assertClass(project, "an9elproject")
    
    digest <- digest::digest(project, "md5")
    last_version_online <- get_versions(project$version$project)
    sion <- project$version$version
    digest_online <- (last_version_online %>%
                      dplyr::filter(to_version == sion))$digest
    if (identical(digest, digest_online)) {
        return(TRUE)
    } else if (identical(digest_online, character(0))) {
        return(digest)
    } else {
        if (verbose) {
            cli::cli_alert_warning("This project is modified")
        }
        return(FALSE)
    }
}
    
