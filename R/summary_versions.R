
##' print number of versions by project
##'
##' print number of versions by project
##' @title print number of versions by project
##' @param plot flag to plot
##' @return data.frame with projects and number of versions
##' @examples
##' summary_versions()
##' @author
##' person("Angel", "Martinez-Perez",
##' email = \email{angelmartinez@protonmail.com},
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' 
##' @export
summary_versions <- function(plot = FALSE) {
    checkmate::assertFlag(plot)
    all <- get_versions() %>%
        dplyr::group_by(project) %>%
        dplyr::summarize(number_of_versions = dplyr::n()) %>%
        dplyr::arrange(desc(number_of_versions))

    if (plot) {
        to_plot <- get_versions() %>%
            dplyr::group_by(project, year = lubridate::ym(paste0(lubridate::year(time), "/",
                                                       month = lubridate::month(time)))) %>%
            dplyr::summarize(sum = dplyr::n()) %>% dplyr::arrange(project, year)
        p <- ggplot2::ggplot(to_plot) +
            ggplot2::geom_bar(ggplot2::aes(x = year,
                                           y = sum,
                                           color = project,
                                           fill = project),
                              stat = "identity" ) +
            ggplot2::scale_x_date("", breaks = "1 month", labels = scales::date_format("%Y-%b")) +
            ggplot2::xlab("") +
            ggplot2::ylab(paste0("number of versions(", sum(to_plot$sum), ")")) +
            ggplot2::coord_flip()
        print(p)
    }
    return(all)
    
}
